## loan -- Calculate and show Loan Details

loan(1) will create a report showing Loan Details over
the length of the loan.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/loan) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/loan.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/loan.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
